package com.equipo8.back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsultaSpeiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultaSpeiApplication.class, args);
	}

}
