package com.equipo8.back;

import com.equipo8.back.movimientos.MovimientoModel;
import com.equipo8.back.movimientos.MovimientoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/consulta")
public class MovimientoController {
    @Autowired
    MovimientoService movimientoService;

    @GetMapping(path = "/general/")
    public ResponseEntity<Object> getMovimientos() {
        List<MovimientoModel> list = movimientoService.findAllMovimientos();
        if (list.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(list);
        }
    }

    @GetMapping(path = "/general/{cuenta}")
    public ResponseEntity<Object> getMovimientosCuenta(@PathVariable String cuenta) {
        List<MovimientoModel> list = movimientoService.findAllMovimientosCuenta(cuenta);
        if (list.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(list);
        }
    }
}
