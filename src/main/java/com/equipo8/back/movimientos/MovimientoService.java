package com.equipo8.back.movimientos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovimientoService {
   @Autowired
    MovimientoRepository movimientoRepository;

   public List<MovimientoModel> findAllMovimientos() {
       return movimientoRepository.findAll();
   }

   public List<MovimientoModel> findAllMovimientosCuenta(String cuenta) {
       return movimientoRepository.findByCuenta(cuenta);
   }
}