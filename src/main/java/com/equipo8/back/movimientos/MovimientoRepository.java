package com.equipo8.back.movimientos;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovimientoRepository extends MongoRepository<MovimientoModel, String> {
    @Query("{cuenta : ?0}")
    public List<MovimientoModel> findByCuenta(String cuenta);
}
